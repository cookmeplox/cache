import io
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 23
archiveNumber = 4

def dump():
	for i, file in enumerate(utils.getFiles(indexNumber, archiveNumber)):
		size = readInt(file)
		b = file.read(size)
		with open('../out/bigger/%s.png' % i, 'wb') as f:
			f.write(b)
