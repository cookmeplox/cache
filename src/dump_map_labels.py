import io
import json
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 2
archiveNumber = 36

opcodes = utils.getOpcodes('opcodes/map_labels.json')

def decode(file):
	obj = {}
	while True:
		op = readUByte(file)
		if op == 0:
			break
		elif op in (9, 20):
			obj['vb%d'%op] = [readUShort(file), readUShort(file), readInt(file), readInt(file)]
		elif op == 26:
			obj['legacySwitch'] = {'unk_1': readUShort(file), 'unk_2': readUShort(file), 'unk_3': readUByte(file), 'index': readUShort(file), 'legacyIndex': readUShort(file)}
		elif op == 15:
			# TODO: clean this up. Happen to be lucky that it's always the end
			obj[15] = list(file.read())
			file.seek(-1, 2)
		elif op in opcodes:
			utils.insert(obj, opcodes[op], file)
		else:
			print('fk', op, list(file.getvalue()[file.tell():]), json.dumps(obj))
			break
	return obj

def get():
	labels = []
	for file in utils.getFiles(indexNumber, archiveNumber):
		labels.append(decode(file))
	return labels
