import json

from collections import Counter

import dump_maps

import utils

with open('../out/objects.json', 'r') as f:
	j = json.load(f)
	objectList = {}
	for i in j:
		objectList[i['id']] = i

indexNumber = 5

TOP = 1
RIGHT = 2
BOTTOM = 4
LEFT = 8

infos = utils.getArchiveInfos(indexNumber)

def getOBSTACLES(regionX, regionY, OBSTACLES, NOMOVE):
	archiveNumber = regionX + regionY*128
	if archiveNumber >= len(infos) or infos[archiveNumber]['childSize'] == 0:
		return False
	files = utils.getFiles(indexNumber, archiveNumber, infos=infos)
	indices = infos[archiveNumber]['childIndices']
	if indices is None:
		indices = list(range(9))
	files = {i: f for i, f in zip(indices, files)}

	if 0 not in files:
		return False
	squares = dump_maps.decodeSquares(files[3])
	objects = dump_maps.decodeObjects(files[0])

	for i in objects:
		x, y, plane = i['localX'], i['localY'], i['plane']
		watery = (squares[1][x][y].get('unk2', 0) & 2) != 0
		if (plane == 0 and not watery) or (plane == 1 and watery):
			if i['type'] == 10:
				stuff = objectList.get(i['objectId'], {})
				# too big to occlude -- castle
				if 'unknown_11' in stuff:
					continue
				height, width = stuff.get('length', 1), stuff.get('width', 1)
				if i['rotation'] % 2 == 1:
					height, width = width, height
				for a in range(width):
					for b in range(height):
						OBSTACLES[64*regionX + x+a, 64*regionY + y+b] |= TOP + RIGHT + BOTTOM + LEFT
			elif i['type'] == 0:
				if i['rotation'] == 0:
					OBSTACLES[64*regionX + x, 64*regionY + y] |= LEFT
				elif i['rotation'] == 1:
					OBSTACLES[64*regionX + x, 64*regionY + y] |= TOP
				elif i['rotation'] == 2:
					OBSTACLES[64*regionX + x, 64*regionY + y] |= RIGHT
				elif i['rotation'] == 3:
					OBSTACLES[64*regionX + x, 64*regionY + y] |= BOTTOM
			elif i['type'] == 9:
				OBSTACLES[64*regionX + x, 64*regionY + y] |= TOP + RIGHT + BOTTOM + LEFT
			elif i['type'] == 2:
				if i['rotation'] == 0:
					OBSTACLES[64*regionX + x, 64*regionY + y] |= TOP + LEFT
				elif i['rotation'] == 1:
					OBSTACLES[64*regionX + x, 64*regionY + y] |= TOP + RIGHT
				elif i['rotation'] == 2:
					OBSTACLES[64*regionX + x, 64*regionY + y] |= RIGHT + BOTTOM
				elif i['rotation'] == 3:
					OBSTACLES[64*regionX + x, 64*regionY + y] |= LEFT + BOTTOM
	for x in range(64):
		for y in range(64):
			if (squares[1][x][y].get('unk2', 0) & 2) == 0:
				if (squares[0][x][y].get('unk2', 0) & 1) != 0:
					NOMOVE.add((64*regionX + x, 64*regionY + y))
			else:
				if (squares[1][x][y].get('unk2', 0) & 1) != 0:
					NOMOVE.add((64*regionX + x, 64*regionY + y))
	return True

start = (0, 0)
end = (99, 199)
# start = (50, 50)
# end = (50, 50)
OBSTACLES = Counter()
NOMOVE = set()
for x in range(start[0], end[0]+1):
	print(x)
	for y in range(start[1], end[1]+1):
		getOBSTACLES(x, y, OBSTACLES, NOMOVE)
print(len(OBSTACLES), len(NOMOVE))

# f = open('obstacles.txt','w')
# for i in OBSTACLES:
# 	f.write("%s\t%s\t%s\n" % (i[0], i[1], OBSTACLES[i]))
# f.close()
# f = open('nomove.txt','w')
# for i in NOMOVE:
# 	f.write("%s\t%s\n" % (i[0], i[1]))
# f.close()