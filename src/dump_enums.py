import io
import json
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 17
FILES_PER_ARCHIVE = 256

def decode(file):
	# TODO: do something with the additional info
	obj = {}
	integer = None
	string = None
	_, bkey = readUByte(file), readUByte(file)
	_, bvalue = readUByte(file), readUByte(file)
	while True:
		op = readUByte(file)
		if op == 0:
			break
		elif op == 4:
			integer = readInt(file)
		elif op == 5:
			count = readUShort(file)
			for _ in range(count):
				key = readInt(file)
				value = readString(file)
				obj[key] = value
		elif op == 6:
			return obj
			count = readUShort(file)
			for _ in range(count):
				key = readInt(file)
				value = readInt(file)
				obj[key] = value
		elif op == 7:
			max = readUShort(file)
			count = readUShort(file)
			for _ in range(count):
				key = readUShort(file)
				value = readString(file)
				obj[key] = value
		elif op == 8:
			max = readUShort(file)
			count = readUShort(file)
			for _ in range(count):
				key = readUShort(file)
				value = readInt(file)
				obj[key] = value
		elif op == 3:
			obj['#string'] = readString(file)
		else:
			raise ValueError(op, file.read())
	return obj

def dump():
	enums = []
	for archiveNumber, files in utils.getAllFiles(indexNumber):
		for fileNumber, file in enumerate(files):
			if len(file.getvalue()) > 1:
				decoded = decode(file)
				n = archiveNumber * 256 + fileNumber
				with open('../out/enums/%d.json' % n, 'w') as f:
					json.dump(decoded, f, indent=2)
