import json

import download
import utils

versionsFilename = '/home/cook/cache/downloaded/versions.json'

with open(versionsFilename, 'r') as f:
	oldVersions = json.load(f)

utils.downloadArchive(255, 255)

top = utils.getTop()

toDownload = []
for indexNumber, index in enumerate(top):
	if index['archives'] == 0:
		continue
	utils.downloadArchive(255, indexNumber, index['version'])
	if oldVersions.get(str(indexNumber)) != index['version']:
		print(oldVersions.get(str(indexNumber)), index['version'])
		toDownload.append(indexNumber)

broke = [40, 47]
for indexNumber in toDownload:
	index = top[indexNumber]
	if index['size'] > 0:
		if indexNumber not in broke:
			print(indexNumber)
			utils.downloadIndex(indexNumber)
			oldVersions[indexNumber] = index['version']

with open(versionsFilename, 'w') as f:
	json.dump(oldVersions, f)