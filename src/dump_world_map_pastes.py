import io
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 23
archiveNumber = 1
 
def decode(file):
	squares = readUShort(file)
	ret = {}
	if squares != 0:
		ret['squares'] = []
		for _ in range(squares):
			obj = {}
			obj['planes'] = readUShort(file)
			obj['original_regionX'] = readUShort(file)
			obj['original_regionY'] = readUShort(file)
			obj['zero'] = readUByte(file)
			obj['new_regionX'] = readUShort(file)
			obj['new_regionY'] = readUShort(file)
			ret['squares'].append(obj)
	chunks = readUShort(file)
	ret['chunks'] = []
	for _ in range(chunks):
		obj = {}
		obj['planes'] = readUShort(file)
		obj['original_regionX'] = readUShort(file)
		obj['original_regionY'] = readUShort(file)
		obj['original_chunkX'] = readUByte(file)
		obj['original_chunkY'] = readUByte(file)
		obj['zero'] = readUByte(file)
		obj['new_regionX'] = readUShort(file)
		obj['new_regionY'] = readUShort(file)
		obj['new_chunkX'] = readUByte(file)
		obj['new_chunkY'] = readUByte(file)
		ret['chunks'].append(obj)
	ret['width'] = readUByte(file)
	ret['height'] = readUByte(file)
	return ret

def get():
	pastes = []
	for file in utils.getFiles(indexNumber, archiveNumber):
		pastes.append(decode(file))
	return pastes
