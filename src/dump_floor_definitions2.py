import io
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 2
archiveNumber = 4

opcodes = utils.getOpcodes('opcodes/floor_definitions2.json')

def decode(file):
	obj = {}
	while True:
		op = readUByte(file)
		if op == 0:
			break
		elif op in opcodes:
			utils.insert(obj, opcodes[op], file)
		else:
			print('fk', op, [i for i in file.read()], obj)
			break
	return obj

def get():
	files = utils.getFiles(indexNumber, archiveNumber)
	return [decode(file) for file in files]
