import io
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 19

opcodes = utils.getOpcodes('opcodes/items.json')

def decode(file):
	obj = {}
	while True:
		op = readUByte(file)
		if op == 0:
			break
		elif op in opcodes:
			utils.insert(obj, opcodes[op], file)
		else:
			print('fk', op, [i for i in file.read()])
			break
	return obj

def get():
	items = []
	for archiveNumber, files in utils.getAllFiles(indexNumber):
		for file in files:
			decoded = decode(file)
			items.append(decoded)
	return items
