import utils

from reads import *

import dump_map_labels

indexNumber = 42

def decode(file):
	objs = []
	count = readUShort(file)
	for _ in range(count):
		obj = {}
		loc = readInt(file)
		obj['y'] = loc & 0x3fff
		obj['x'] = (loc >> 14) & 0x3fff
		obj['plane'] = loc >> 28
		obj['labelId'] = readUShort(file)
		readUByte(file) # always 0
		objs.append(obj)
	return objs

def get():
	locs = []
	for archiveNumber, archive in utils.getArchives(indexNumber):
		locations = decode(archive)
		locs.append(locations)
	return locs
