import gzip
import io
import json
import os
import os.path
import sqlite3
import struct
import time
import zlib

import download

from reads import *

BASE_FOLDER = '../'
CACHE_FOLDER = '/Users/jonathanlee/Jagex/RuneScape/'
DOWNLOAD_FOLDER = BASE_FOLDER + 'downloaded/'
DOWNLOAD = False

def getIndexFilename(indexNumber):
	return CACHE_FOLDER + 'js5-%d.jcache' % indexNumber

def getDownloadFilename(indexNumber, archiveNumber, timestamp):
	if timestamp is None:
		timestamp = 'current'
	return "%s%s/%s/%s.gz" % (DOWNLOAD_FOLDER, indexNumber, archiveNumber, timestamp)

def getValidIndexes():
	files = set(os.listdir(CACHE_FOLDER))
	return {indexNumber for indexNumber in range(256) if 'js5-%d.jcache' % indexNumber in files}

broken = {(255, 47), (14, 0)}

def downloadArchive(indexNumber, archiveNumber, timestamp=None):
	if (indexNumber, archiveNumber) in broken:
		return
	if timestamp is None:
		timestamp = int(time.time())
	filename = getDownloadFilename(indexNumber, archiveNumber, timestamp)
	if not os.path.exists(os.path.dirname(filename)):
	        os.makedirs(os.path.dirname(filename))
	if not os.path.isfile(filename):
		download.downloadArchive(indexNumber, archiveNumber, filename)
		if not os.path.exists(filename):
			return
		link =  getDownloadFilename(indexNumber, archiveNumber, None)
		if os.path.exists(link):
			os.unlink(link)
		os.symlink(filename, link)

def downloadIndex(indexNumber):
	infos = getArchiveInfos(indexNumber)
	files = []
	for archiveNumber, val in enumerate(infos):
		if (indexNumber, archiveNumber) in broken:
			continue
		filename = getDownloadFilename(indexNumber, archiveNumber, val['version'])
		if not os.path.isfile(filename):
			if val['compressedSize'] > 0 or val['crc'] != 0:
				files.append((indexNumber, archiveNumber, filename, val['compressedSize']))
				if not os.path.exists(os.path.dirname(filename)):
					os.makedirs(os.path.dirname(filename))
	download.batchDownload(files)
	for indexNumber, archiveNumber, filename, size in files:
		if not os.path.exists(filename):
			continue
		link =  getDownloadFilename(indexNumber, archiveNumber, None)
		if os.path.exists(link):
			os.unlink(link)
		os.symlink(filename, link)

def getTop():
	SIZE_PER_INDEX = 80
	file = getArchive(255, 255)
	count = readUByte(file)
	indexes = []
	for _ in range(count):
		obj = {}
		obj['crc'] = readInt(file)
		obj['version'] = readInt(file)
		obj['archives'] = readInt(file)
		obj['size'] = readInt(file)
		file.seek(SIZE_PER_INDEX - 16, 1)
		indexes.append(obj)
	return indexes

def getArchives(indexNumber, timestamp=None):
	if DOWNLOAD:
		top = getTop()
		infos = getArchiveInfos(indexNumber)
		for archiveNumber, info in enumerate(infos):
			if info['compressedSize'] > 0 or info['crc'] != 0:
				yield archiveNumber, getArchive(indexNumber, archiveNumber, timestamp)
	else:
		if indexNumber not in getValidIndexes():
			raise ValueError("No file found for index %d" % indexNumber)

		indexFilename = getIndexFilename(indexNumber)
		c = sqlite3.connect(indexFilename)
		for key, data in c.execute('SELECT KEY, DATA FROM cache'):
			uncompressed = zlib.decompress(memoryview(data)[8:])
			yield key, io.BytesIO(uncompressed)

def getArchive(indexNumber, archiveNumber, timestamp=None):
	if DOWNLOAD:
		filename = getDownloadFilename(indexNumber, archiveNumber, timestamp)
		with gzip.open(filename, 'rb') as f:
			return io.BytesIO(f.read())
	else:
		if indexNumber not in getValidIndexes():
			raise ValueError("No file found for index %d" % indexNumber)

		indexFilename = getIndexFilename(indexNumber)
		c = sqlite3.connect(indexFilename)
		for key, data in c.execute('SELECT KEY, DATA FROM cache WHERE KEY=?', [archiveNumber]):
			uncompressed = zlib.decompress(memoryview(data)[8:])
			c.close()
			return io.BytesIO(uncompressed)
		raise ValueError("Archive %d not found in index %d" % (archiveNumber, indexNumber))

def getArchiveIndex(indexNumber, timestamp=None):
	if DOWNLOAD:
		filename = getDownloadFilename(255, indexNumber, timestamp)
		with gzip.open(filename, 'rb') as f:
			return io.BytesIO(f.read())
	indexFilename = getIndexFilename(indexNumber)
	c = sqlite3.connect(indexFilename)
	for data, in c.execute('SELECT DATA FROM cache_index'):
		data = zlib.decompress(memoryview(data)[8:])
		f = io.BytesIO(data)
		return f

def getArchiveInfos(indexNumber):
	f = getArchiveIndex(indexNumber)

	# actually doing stuff with the data...
	format = readByte(f)
	version = readInt(f) if format > 5 else 0
	attributes = readUByte(f)
	named = (attributes & 0x01) != 0
	hashed = (attributes & 0x02) != 0
	unk4 = (attributes & 0x04) != 0
	unk8 = (attributes & 0x08) != 0
	entries = readSmart32(f) if format >= 7 else readUShort(f)
	indexMap = [0 for _ in range(entries)]
	maxOffset = 0
	offset = 0
	for i in range(entries):
		offset += readSmart32(f) if format >= 7 else readUShort(f)
		indexMap[i] = offset
		if offset > maxOffset:
			maxOffset = offset
	size = maxOffset + 1

	rows = [{'name': -1,
			 'crc': 0,
		     'unk1': -1,
		     'compressedSize': 0,
		     'size': 0,
		     'version': 0,
		     'childCount': 0,
		     'childSize': 0,
		     'childIndices': [],
			 'digests': None} for _ in range(size)]

	if named:
		for i in range(entries):
			rows[indexMap[i]]['name'] = readInt(f)

	for i in range(entries):
		rows[indexMap[i]]['crc'] = readInt(f)

	if unk4:
		for i in range(entries):
			rows[indexMap[i]]['unk1'] = readInt(f)

	if hashed:
		for i in range(entries):
			rows[indexMap[i]]['digest'] = readBytes(f, 64)

	if unk4:
		for i in range(entries):
			rows[indexMap[i]]['compressedSize'] = readInt(f)
			rows[indexMap[i]]['size'] = readInt(f)

	for i in range(entries):
		rows[indexMap[i]]['version'] = readInt(f)

	for i in range(entries):
		rows[indexMap[i]]['childCount'] = readSmart32(f) if format >= 7 else readUShort(f)

	for i in range(entries):
		count = rows[indexMap[i]]['childCount']
		offset = 0
		rows[indexMap[i]]['childIndices'] = [0 for _ in range(count)]

		maxOffset = 0
		for j in range(count):
			offset += readSmart32(f) if format >= 7 else readUShort(f)
			rows[indexMap[i]]['childIndices'][j] = offset
			if offset > maxOffset:
				maxOffset = offset

		rows[indexMap[i]]['childSize'] = maxOffset + 1
		if maxOffset + 1 == count:
			rows[indexMap[i]]['childIndices'] = None
	return rows

def getOffsets(archive, count):
	if DOWNLOAD:
		archive.seek(-4 * count - 1, 2)
		offsets = [0]
		prev = 0
		for _ in range(count):
			prev += readSignedInt(archive)
			offsets.append(offsets[-1] + prev)
		return offsets
	else:
		archive.seek(1)
		offsets = []
		while True:
			if len(offsets) > 0 and archive.tell() >= offsets[0]:
				break
			offset = readInt(archive)
			offsets.append(offset)
		return offsets

def getFilesHelper(archive, info):
	files = []
	if len(archive.getvalue()) == 0:
		return files
	offsets = getOffsets(archive, info['childCount'])
	archive.seek(offsets[0])
	for start, end in zip(offsets[:-1], offsets[1:]):
		bytes = archive.read(end - start)
		files.append(io.BytesIO(bytes))
	return files

def getFiles(indexNumber, archiveNumber, infos=None):
	archive = getArchive(indexNumber, archiveNumber)
	if infos is None:
		infos = getArchiveInfos(indexNumber)
	return getFilesHelper(archive, infos[archiveNumber])

def getAllFiles(indexNumber):
	archives = getArchives(indexNumber)
	infos = getArchiveInfos(indexNumber)
	for archiveNumber, archive in archives:
		yield archiveNumber, getFilesHelper(archive, infos[archiveNumber])

def getOpcodes(filename):
	filename = filename.split('/')[-1]
	filename = BASE_FOLDER + 'opcodes/' + filename
	with open(filename, 'r') as f:
		raw = json.load(f)
	return {o['opcode']: o for o in raw}

def getValue(opcode, file):
	if 'type' in opcode:
		if opcode['type'] == 'Array':
			count = getValue(opcode['count'], file)
			return [getValue(opcode['element_type'], file) for _ in range(count)]
		elif opcode['type'] == 'KVArray':
			count = getValue(opcode['count'], file)
			KVArray = {}
			for _ in range(count):
				key = getValue(opcode['key_type'], file)
				value = getValue(opcode['element_type'], file)
				KVArray[key] = value
			return KVArray
		elif opcode['type'] == 'Structure':
			struct = {}
			for field in opcode['fields']:
				struct[field['name']] = getValue(field, file)
			return struct
		else:
			raise ValueError('Unsupported complex type: %s' % opcode['type'])
	else:
		func = read(opcode['readType'])
		return func(file)

def insert(obj, opcode, file):
	value = getValue(opcode, file)
	if 'name' in opcode:
		name = opcode['name']
	else:
		name = 'unk_%s' % opcode['opcode']
	if 'index' in opcode:
		index = opcode['index']
		if name not in obj:
			obj[name] = []
		missing = index - len(obj[name]) + 1
		obj[name].extend([None for _ in range(missing)])
		obj[name][index] = value
	else:
		if name in obj:
			# duplicate opcode; overwrite but track
			key = '#' + name
			if key not in obj:
				obj[key] = []
			obj[key].append(obj[name])
		obj[name] = value
