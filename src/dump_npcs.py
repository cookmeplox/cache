import io
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 18

opcodes = utils.getOpcodes('opcodes/npcs.json')

def decode(file):
	obj = {}
	while True:
		op = readUByte(file)
		if op == 0:
			break
		elif op in opcodes:
			utils.insert(obj, opcodes[op], file)
		else:
			print('fk', op, [i for i in file.read()], obj)
			break
	return obj

def get():
	npcs = []
	for archiveNumber, files in utils.getAllFiles(indexNumber):
		for file in files:
			decoded = decode(file)
			npcs.append(decoded)
	return npcs
