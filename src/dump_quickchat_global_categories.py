import io
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 25
archiveNumber = 0

opcodes = utils.getOpcodes('opcodes/quickchat_categories.json')

def decode(file):
	obj = {}
	while True:
		op = readUByte(file)
		if op == 0:
			break
		elif op in opcodes:
			utils.insert(obj, opcodes[op], file)
		else:
			print('fk', op, [i for i in file.read()], obj)
			break
	return obj

def get():
	qc = []
	for file in utils.getFiles(indexNumber, archiveNumber):
		decoded = decode(file)
		qc.append(decoded)
	return qc
