import json

# import dump_cursors
# import dump_floor_definitions
# import dump_floor_definitions2
import dump_items
import dump_invention
import dump_map_labels
import dump_map_sprites
import dump_npcs
import dump_objects
import dump_quests
import dump_quickchat_categories
import dump_quickchat
import dump_quickchat_global_categories
import dump_quickchat_global
# import dump_world_map_bigger
# import dump_world_map_mini
# import dump_world_map_pastes
# import dump_world_map_zones

# import dump_enums
# import dump_structs
# import dump_sprites

def dump(module, filename):
	contents = module.get()
	filename = '../out/' + filename
	with open(filename, 'w') as f:
		json.dump(contents, f, indent=2)

# dump(dump_cursors, 'cursors.json')
# dump(dump_floor_definitions, 'floor_definitions.json')
# dump(dump_floor_definitions2, 'floor_definitions2.json')
# dump(dump_invention, 'invention.json')
dump(dump_items, 'items.json')
# dump(dump_map_labels, 'map_labels.json')
# dump(dump_map_sprites, 'map_sprites.json')
# dump(dump_npcs, 'npcs.json')
# dump(dump_objects, 'objects.json')
# dump(dump_quests, 'quests.json')
# dump(dump_quickchat, 'quickchat.json')
# dump(dump_quickchat_categories, 'quickchat_categories.json')
# dump(dump_quickchat_global, 'quickchat_global.json')
# dump(dump_quickchat_global_categories, 'quickchat_global_categories.json')
# dump(dump_world_map_pastes, 'world_map_pastes.json')
# dump(dump_world_map_zones, 'world_map_zones.json')

dump_enums.dump()
dump_structs.dump()
# dump_world_map_mini.dump()
# dump_world_map_bigger.dump()
# dump_sprites.dump()
# maps
# sprites
