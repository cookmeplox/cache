import io
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 40

def get():
	for archiveNumber, archive in utils.getArchives(indexNumber):
		with open('out/music/%d.ogg' % archiveNumber, 'wb') as f:
			f.write(archive.getvalue())
