import io
import json
import zlib
import sqlite3

from reads import *
import utils

indexNumber = 22
FILES_PER_ARCHIVE = 32

def decode(file):
	# TODO: do something with the additional info
	while True:
		op = readUByte(file)
		if op == 0:
			break
		elif op == 249:
			obj = readTable(file)
		else:
			raise ValueError(op, file.read())
	return obj

def dump():
	enums = []
	for archiveNumber, files in utils.getAllFiles(indexNumber):
		for fileNumber, file in enumerate(files):
			if len(file.getvalue()) > 1:
				decoded = decode(file)
				n = archiveNumber * FILES_PER_ARCHIVE + fileNumber
				with open('../out/structs/%d.json' % n, 'w') as f:
					json.dump(decoded, f, indent=2)
