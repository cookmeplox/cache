import io
import json
import os.path
import zlib
import sqlite3

from PIL import Image, ImageDraw

from reads import *
import utils

import dump_floor_definitions
import dump_floor_definitions2
import dump_map_labels
import dump_map_label_locations
import dump_map_sprites
import dump_maps

"""
TODO:
Better moving average (constant time)
Figure out what makes a door a door (e.g. red)
Clean up stray walls that don't belong
Clean up walls in water/arodund big objects, that don't belong
Ingame map sprites (e.g) ladders look blurrier.
"""

indexNumber = 5
LINECOLOR = (255, 255, 255)
FIDELITY = 4
ZOOM = 1
PLANE = 2
INTERP = 5
NEW_MINIMAP_ICONS = True
SHOW_WATER = False

filename_format = "../out/maps/%s_%d_%d_%d.png"
if SHOW_WATER:
	OBJFILE, SQUAREFILE = 1, 4
else:
	OBJFILE, SQUAREFILE = 0, 3

LINEWIDTH = 1 * FIDELITY
SCALE = int(FIDELITY * ZOOM * 4)

mapscenesCache = {}
minimapIconsCache = {}

with open('../out/objects.json', 'r') as f:
	j = json.load(f)
	objectList = {}
	for i in j:
		objectList[i['id']] = i

# for i in objectList.values():
# 	if 'unknown_be' in i:
# 		print(i['unknown_be'], '\t', i.get('name'))
# 1/0
floors = dump_floor_definitions.get()
floors2 = dump_floor_definitions2.get()
map_labels = dump_map_labels.get()
map_sprites = dump_map_sprites.get()
map_label_locations = dump_map_label_locations.get()

label_locations_by_square = {}
for location in map_label_locations:
	for map_label in location:
		square = (map_label['x']//64, map_label['y']//64)
		if square not in label_locations_by_square:
			label_locations_by_square[square] = []
		label_locations_by_square[square].append(map_label)

def getMinimapIconSprite(mapFunction, map_labels, report=True):
	if 'iconSprite' in map_labels[mapFunction]:
		return map_labels[mapFunction]['iconSprite']
	if 'legacySwitch' in map_labels[mapFunction]:
		if NEW_MINIMAP_ICONS:
			index = map_labels[mapFunction]['legacySwitch']['index']
		else:
			index = map_labels[mapFunction]['legacySwitch']['legacyIndex']
		if 'iconSprite' in map_labels[index]:
			return map_labels[index]['iconSprite']
	if report:
		print("Nothing to do with %s: (%s)" % (mapFunction, map_labels[mapFunction]))

def drawOverlayTile(draw, shape, fill, underfill, x, y):
	left = x*SCALE
	top = (64-y-1)*SCALE
	right = left + SCALE - 1
	bottom = top + SCALE - 1
	if fill == (255, 0, 255):
		# This isn't quite the right thing to do
		fill = (0, 0, 0)
	if shape == 0:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
	elif shape == 4:
		draw.polygon([(left, top), (left, bottom), (right, bottom)], fill=fill)
	elif shape == 5:
		draw.polygon([(left, top), (left, bottom), (right, top)], fill=fill)
	elif shape == 6:
		draw.polygon([(left, top), (right, top), (right, bottom)], fill=fill)
	elif shape == 7:
		draw.polygon([(left, bottom), (right, bottom), (right, top)], fill=fill)
	elif shape == 8:
		draw.polygon([(left, top), ((left+right)/2, top), (left, bottom)], fill=fill)
	elif shape == 9:
		draw.polygon([(left, top), (right, top), (right, (top+bottom)/2)], fill=fill)
	elif shape == 10:
		draw.polygon([(right, top), (right, bottom), ((left+right)/2, bottom)], fill=fill)
	elif shape == 11:
		draw.polygon([(right, bottom), (left, bottom), (left, (top+bottom)/2)], fill=fill)
	elif shape == 12:
		draw.polygon([(right, top), ((left+right)/2, top), (right, bottom)], fill=fill)
	elif shape == 13:
		draw.polygon([(left, bottom), (right, bottom), (right, (top+bottom)/2)], fill=fill)
	elif shape == 14:
		draw.polygon([(left, top), (left, bottom), ((left+right)/2, bottom)], fill=fill)
	elif shape == 15:
		draw.polygon([(left, top), (right, top), (left, (top+bottom)/2)], fill=fill)
	elif shape == 16:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(left, top), ((left+right)/2, top), (left, bottom)], fill=underfill)
	elif shape == 17:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(left, top), (right, top), (right, (top+bottom)/2)], fill=underfill)
	elif shape == 18:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(right, top), (right, bottom), ((left+right)/2, bottom)], fill=underfill)
	elif shape == 19:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(right, bottom), (left, bottom), (left, (top+bottom)/2)], fill=underfill)
	elif shape == 20:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(right, top), ((left+right)/2, top), (right, bottom)], fill=underfill)
	elif shape == 21:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(left, bottom), (right, bottom), (right, (top+bottom)/2)], fill=underfill)
	elif shape == 22:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(left, top), (left, bottom), ((left+right)/2, bottom)], fill=underfill)
	elif shape == 23:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(left, top), (right, top), (left, (top+bottom)/2)], fill=underfill)
	elif shape == 24:
		draw.polygon([(left, top), (left, bottom), ((left+right)/2, bottom), ((left+right)/2, top)], fill=fill)
	elif shape == 25:
		draw.polygon([(left, top), (left, (top+bottom)/2), (right, (top+bottom)/2), (right, top)], fill=fill)
	elif shape == 26:
		draw.polygon([(right, top), (right, bottom), ((left+right)/2, bottom), ((left+right)/2, top)], fill=fill)
	elif shape == 27:
		draw.polygon([(left, bottom), (left, (top+bottom)/2), (right, (top+bottom)/2), (right, bottom)], fill=fill)
	elif shape == 28:
		draw.polygon([(left, bottom), (left, (top+bottom)/2), ((left+right)/2, bottom)], fill=fill)
	elif shape == 29:
		draw.polygon([(left, top), (left, (top+bottom)/2), ((left+right)/2, top)], fill=fill)
	elif shape == 30:
		draw.polygon([(right, top), (right, (top+bottom)/2), ((left+right)/2, top)], fill=fill)
	elif shape == 31:
		draw.polygon([(right, bottom), (right, (top+bottom)/2), ((left+right)/2, bottom)], fill=fill)
	elif shape == 32:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(left, bottom), (left, (top+bottom)/2), ((left+right)/2, bottom)], fill=underfill)
	elif shape == 33:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(left, top), (left, (top+bottom)/2), ((left+right)/2, top)], fill=underfill)
	elif shape == 34:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(right, top), (right, (top+bottom)/2), ((left+right)/2, top)], fill=underfill)
	elif shape == 35:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(right, bottom), (right, (top+bottom)/2), ((left+right)/2, bottom)], fill=underfill)
	# these ones might not be correct totally...
	elif shape == 36:
		draw.polygon([(left, top), (left, bottom), (right, top)], fill=(fill))
	elif shape == 37:
		draw.polygon([(left, top), (right, top), (right, bottom)], fill=fill)
	elif shape == 38:
		draw.polygon([(right, top), (right, bottom), (left, bottom)], fill=fill)
	elif shape == 39:
		draw.polygon([(left, top), (left, bottom), (right, bottom)], fill=fill)
	elif shape == 40:
		draw.polygon([(left, bottom), (right, bottom), (right, top)], fill=(fill))
	elif shape == 41:
		draw.polygon([(left, top), (left, bottom), (right, bottom)], fill=fill)
	elif shape == 42:
		draw.polygon([(left, top), (left, bottom), (right, top)], fill=fill)
	elif shape == 43:
		draw.polygon([(left, top), (right, top), (right, bottom)], fill=fill)
	elif shape == 44:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(right, bottom), (right, (top+bottom)/2), ((left+right)/2, bottom)], fill=underfill)
	elif shape == 45:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(left, bottom), (left, (top+bottom)/2), ((left+right)/2, bottom)], fill=underfill)
	elif shape == 46:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(left, top), (left, (top+bottom)/2), ((left+right)/2, top)], fill=underfill)
	elif shape == 47:
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=fill)
		draw.polygon([(right, top), (right, (top+bottom)/2), ((left+right)/2, top)], fill=underfill)
	else:
		# not recognized
		draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=(255, 0, 0))
		print(shape, "no matching tile shape...yet")

def drawLine(draw, type, rotation, x, y, fill=LINECOLOR):
	left = x*SCALE
	top = (64-y-1)*SCALE
	right = left + SCALE - 1
	bottom = top + SCALE - 1
	if type == 0:
		if rotation == 0:
			draw.line([(left, top), (left, bottom)], fill=fill, width=LINEWIDTH)
		elif rotation == 1:
			for a in range(SCALE):
				draw.line([(left, top), (right, top)], fill=fill, width=LINEWIDTH)
		elif rotation == 2:
			for a in range(SCALE):
				draw.line([(right, top), (right, bottom)], fill=fill, width=LINEWIDTH)
		elif rotation == 3:
			for a in range(SCALE):
				draw.line([(left, bottom), (right, bottom)], fill=fill, width=LINEWIDTH)
	if type == 9:
		if rotation == 0:
			draw.line([(left, bottom), (right, top)], fill=fill, width=LINEWIDTH)
		elif rotation == 1:
			draw.line([(left, top), (right, bottom)], fill=fill, width=LINEWIDTH)
		elif rotation == 2:
			draw.line([(left, bottom), (right, top)], fill=fill, width=LINEWIDTH)
		elif rotation == 3:
			draw.line([(left, top), (right, bottom)], fill=fill, width=LINEWIDTH)
	if type == 2:
		if rotation == 0:
			draw.line([(left, top), (right, top)], fill=fill, width=LINEWIDTH)
			draw.line([(left, top), (left, bottom)], fill=fill, width=LINEWIDTH)
		elif rotation == 1:
			draw.line([(left, top), (right, top)], fill=fill, width=LINEWIDTH)
			draw.line([(right, top), (right, bottom)], fill=fill, width=LINEWIDTH)
		elif rotation == 2:
			pass
			draw.line([(right, top), (right, bottom)], fill=fill, width=LINEWIDTH)
			draw.line([(left, bottom), (right, bottom)], fill=fill, width=LINEWIDTH)
		elif rotation == 3:
			draw.line([(left, top), (left, bottom)], fill=fill, width=LINEWIDTH)
			draw.line([(left, bottom), (right, bottom)], fill=fill, width=LINEWIDTH)

def getAverageUnderlay(underlays, x, y, usedef=False):
	# average surrounding underlays.
	# This can be done WAY more efficiently by not recomputing everything,
	# but leaving as-is for educational purposes
	# TODO: figure out exact averaging technique...this ain't it (closer matters more?)
	rgb = [0, 0, 0]
	count = 0
	if (not usedef) and (x, y) not in underlays:
		return (0, 0, 0)
	for a in range(-INTERP, INTERP+1):
		for b in range(-INTERP, INTERP+1):
			key = (x+a, y+b)
			# TODO: this might not be the right logic if not found
			if usedef:
				if  (0 < x+a < 64) and (0 < y+b < 64):
					sq = underlays[x+a][y+b]
					if 'underlayTile' in sq:
						underfill = floors[sq['underlayTile']-1]['unk_1']
						rgb[0] += underfill[0]
						rgb[1] += underfill[1]
						rgb[2] += underfill[2]
						count += 1
			else:
				if key in underlays:
					underfill = underlays[key]
					rgb[0] += underfill[0]
					rgb[1] += underfill[1]
					rgb[2] += underfill[2]
				count += 1
	if count > 0:
		return tuple([c//count for c in rgb])
	return (0, 0, 0)

def addMapscene(mapscene, im, x, y):
	if 'spriteId' in map_sprites[mapscene]:
		if mapscene not in mapscenesCache:
			spriteId = map_sprites[mapscene]['spriteId']
			sprite = Image.open("../out/sprites/%d-0.png" % spriteId)
			sprite = sprite.convert('RGBA')
			pixels = sprite.load()
			width, height = sprite.size
			for a in range(width):
			    for b in range(height):
			    	# TODO: find less shitty way to do this
			    	if sum(pixels[a, b][:3]) < 12:
			    		pixels[a, b] = (255, 255, 255, 0)
			mapscenesCache[mapscene] = sprite.resize((SCALE*width//4, SCALE*height//4))
		posX, posY = int(x*SCALE), int((64-y-1.75)*SCALE)
		if posX < 0 or posY < 0:
			# can't fit entirely on this map square -- need to be smarter when we stitch a bunch together
			return
		im.alpha_composite(mapscenesCache[mapscene], (posX, posY))

def addMinimapIcon(spriteId, im, x, y):
	if spriteId in (22449, 21084):
		return
	if spriteId not in minimapIconsCache:
		sprite = Image.open("../out/sprites/%d-0.png" % spriteId)
		sprite = sprite.convert('RGBA')
		pixels = sprite.load()
		width, height = sprite.size
		if not NEW_MINIMAP_ICONS:
			for y in range(height):
			    for x in range(width):
			    	# TODO: find less shitty way to do this
			    	if sum(pixels[x, y][:3]) < 12:
			    		pixels[x, y] = (255, 255, 255, 0)
		minimapIconsCache[spriteId] = sprite.resize(((FIDELITY*width, FIDELITY*height)))
	sprite = minimapIconsCache[spriteId]
	width, height = sprite.size
	posX, posY = int((x-2.5/ZOOM)*SCALE), -height+int((64-y)*SCALE)
	if posX < 0 or posY < 0:
		# can't fit entirely on this map square -- need to be smarter when we stitch a bunch together
		return
	im.alpha_composite(sprite, (posX, posY))

def renderImage(startX, startY, endX=None, endY=None, filename=None):
	if endX is None:
		endX = startX
	if endY is None:
		endY = startY
	infos = utils.getArchiveInfos(indexNumber)
	fileMap = {}
	squaresMap = {} # because we already decoded it
	underlayColors = {}
	for regionX in range(startX, endX+1):
		for regionY in range(startY, endY+1):
			print(regionX, regionY)
			archiveNumber = regionY*128+regionX
			if archiveNumber >= len(infos) or infos[archiveNumber]['childSize'] == 0:
				continue
			files = utils.getFiles(indexNumber, archiveNumber, infos=infos)
			indices = infos[archiveNumber]['childIndices']
			if indices is None:
				indices = list(range(9))
			files = {i: f for i, f in zip(indices, files)}
			fileMap[regionX, regionY] = files
			if SQUAREFILE in files:
				squares = dump_maps.decodeSquares(files[SQUAREFILE], nplanes=(1 if SHOW_WATER else 4))
				files[SQUAREFILE].seek(0)
			else:
				squares = []
			for x in range(64):
				for y in range(64):
					if len(squares) <= PLANE:
						continue
					sq = squares[PLANE][x][y]
					if 'underlayTile' in sq:
						underfill = floors[sq['underlayTile']-1]['unk_1']
						underlayColors[regionX*64+x, regionY*64+y] = underfill
	# todo: paralellize
	regionX = max(x for x, y in fileMap)
	regionY = max(y for x, y in fileMap)
	for regionX in range(startX, endX+1):
		for regionY in range(startY, endY+1):
			if (regionX, regionY) not in fileMap:
				continue
			print(regionX, regionY)
			renderSquare(
				regionX,
				regionY,
				fileMap[regionX, regionY],
				underlayColors
			)
	if filename is not None:
		stitch(filename, startX, startY, endX, endY)

def stitch(filename, startX, startY, endX=None, endY=None):
	if endX is None:
		endX = startX
	if endY is None:
		endY = startY
	sizeX, sizeY = ZOOM*4*64*(endX-startX+1), (ZOOM*4*64*(endY-startY+1))
	im = Image.new("RGBA", (sizeX, sizeY))
	for x in range(startX, endX+1):
		for y in range(startY, endY+1):
			name = filename_format % (ZOOM, PLANE, x, y)
			if os.path.isfile(name): 
				inner = Image.open(name)
				squareSize = ZOOM * 4 * 64
				im.paste(inner, box=((x-startX)*squareSize, squareSize*(endY-startY)-(y-startY)*squareSize))
	im.save(filename, "PNG")

def renderSquare(regionX, regionY, files, underlayColors):
	im = Image.new("RGBA", (64*SCALE, 64*SCALE))
	pix = im.load()
	draw = ImageDraw.Draw(im)
	if SQUAREFILE in files:
		squares = dump_maps.decodeSquares(files[SQUAREFILE], nplanes=(1 if SHOW_WATER else 4))
	else:
		squares = []
	if len(squares) > PLANE:
		for x in range(64):
			for y in range(64):
				left = x*SCALE
				top = (64-y-1)*SCALE
				right = left + SCALE - 1
				bottom = top + SCALE - 1
				underfill = getAverageUnderlay(underlayColors, regionX*64+x, regionY*64+y)
				draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=underfill)
				thing = squares[PLANE][x][y]
				if 'overlayTile' in thing:
					shape = thing['overlayShape']
					floordef = floors2[thing['overlayTile']-1]
					if (thing.get('unk2', 0) & 8) == 0:
						if (thing.get('unk2', 0) & 2) == 0:
							if 'unk_1' in floordef:
								fill = tuple(floordef['unk_1'])
								drawOverlayTile(draw, shape, fill, underfill, x, y)
							if 'unk_7' in floordef:
								fill = tuple(floordef['unk_7'])
								drawOverlayTile(draw, shape, fill, underfill, x, y)
				if PLANE == 0 and not SHOW_WATER:
					for i in range(1, 4):
						highthing = squares[i][x][y]
						if (highthing.get('unk2', 0) & 8) != 0:

							if 'overlayTile' in highthing:
								floordef = floors2[highthing['overlayTile']-1]
								shape = highthing['overlayShape']
								if 'unk_1' in floordef:
									fill = tuple(floordef['unk_1'])
									if fill != (255, 0, 255):
										drawOverlayTile(draw, shape, fill, underfill, x, y)
								if 'unk_7' in floordef:
									fill = tuple(floordef['unk_7'])
									drawOverlayTile(draw, shape, fill, underfill, x, y)
				if not SHOW_WATER and (squares[1][x][y].get('unk2', 0) & 2) != 0 and PLANE != 3:
					highthing = squares[PLANE+1][x][y]
					if 'underlayTile' in highthing:
						floordef = floors[highthing['underlayTile']-1]
						# underfill = tuple(floors[underlay[x][y]['underlayTile']-1]['unk_1'])
						underfill = getAverageUnderlay(squares[PLANE+1], x, y, usedef=True)
						draw.polygon([(left, top), (left, bottom), (right, bottom), (right, top)], fill=underfill)
					if 'overlayTile' in highthing:
						shape = highthing['overlayShape']
						floordef = floors2[highthing['overlayTile']-1]
						if 'unk_7' in floordef:
							fill = tuple(floordef['unk_7'])
							drawOverlayTile(draw, shape, fill, underfill, x, y)
	if OBJFILE in files:
		decodedObjects = [o for o in dump_maps.decodeObjects(files[OBJFILE])]
	else:
		decodedObjects = []
	# walls and such
	for obj in decodedObjects:
		x, y, plane = obj['localX'], obj['localY'], obj['plane']
		watery = (squares[1][x][y].get('unk2', 0) & 2) != 0
		if watery:
			if obj['plane'] == PLANE+1 or (obj['plane'] == 0 and PLANE == 0):
				pass
			else:
				continue
		elif PLANE == 0 and (squares[plane][x][y].get('unk2', 0) & 8) != 0:
			pass
		elif PLANE != 0 and (squares[plane][x][y].get('unk2', 0) & 8) != 0:
			continue
		else:
			if obj['plane'] != PLANE:
				continue

		if obj['type'] in (0, 2, 9):
			fill = LINECOLOR
			if obj['objectId'] in objectList:
				if 'unknown_be' in objectList[obj['objectId']]:
					fill = (255, 0, 0)
			drawLine(draw, obj['type'], obj['rotation'], x, y, fill=fill)
	#trees, rocks, ladders, oh my
	for obj in decodedObjects:
		if obj['plane'] != PLANE:
			continue
		if obj['objectId'] in objectList:
			if 'mapscene' in objectList[obj['objectId']]:
				mapscene = objectList[obj['objectId']]['mapscene']
				addMapscene(mapscene, im, obj['localX'], obj['localY'])
	# object-coded minimap icons
	for obj in decodedObjects:
		if obj['plane'] != PLANE:
			continue
		if obj['objectId'] in objectList:
			if 'mapFunction' in objectList[obj['objectId']]:
				minimapIcon = objectList[obj['objectId']]['mapFunction']
				spriteId = getMinimapIconSprite(minimapIcon, map_labels)
				if spriteId is not None:
					addMinimapIcon(spriteId, im, obj['localX'], obj['localY'])
	# minimap icons that depend on varbits (stores that open later, quests, etc)
	for labelInfo in label_locations_by_square.get((regionX, regionY), []):
		if labelInfo['plane'] != PLANE:
			continue
		label = map_labels[labelInfo['labelId']]
		spriteId = getMinimapIconSprite(labelInfo['labelId'], map_labels, report=False)
		if spriteId is not None:
			addMinimapIcon(spriteId, im, labelInfo['x']%64, labelInfo['y']%64)
	squareSize = 4 * 64 * ZOOM
	im = im.resize((squareSize, squareSize), Image.ANTIALIAS)
	name = filename_format % (ZOOM, PLANE, regionX, regionY)
	# name = "../out/maps/temp.png"
	im.save(name, "PNG")
# renderImage(0, 0, 99, 199)
renderImage(4480//64, 8960//64, 4800//64, 9280//64)
# renderImage(50, 53, filename="hey2.png")
