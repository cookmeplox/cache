import os
import shutil
from PIL import Image

OLD_DIR = '/home/cook/cache/out/maps/'
NEW_DIR = '/tools/maps/tiles/'
PLANE = 3

for i in os.listdir(OLD_DIR):
	if i.startswith("1_"):
		new = "2_" + i[2:]
		shutil.copy(OLD_DIR + i, NEW_DIR + new)

for scale in range(2, -3, -1):
	sz = 2
	size = (256//sz, 256//sz)
	needed = set()
	files = set(os.listdir(NEW_DIR))
	for i in files:
		if '.png' not in i:
			continue
		z, plane, x, y = map(int, i.split(".")[0].split("_"))
		if z != scale:
			continue
		if plane != PLANE:
			continue
		# print(x, y)
		needed.add((x//sz, y//sz, plane))
	# needed = ((25, 25),)
	print(scale, len(needed))
	for x, y, plane in sorted(list(needed)):
		im = Image.new("RGB", (256, 256))
		name = "%s_%s_%s_%s.png" % (scale, plane, 2*x, 2*y)
		if name in files:
			inner = Image.open(NEW_DIR + name)
			inner.thumbnail(size, Image.ANTIALIAS)
			im.paste(inner, box=(0, 128))
		name = "%s_%s_%s_%s.png" % (scale, plane, 2*x+1, 2*y)
		if name in files:
			inner = Image.open(NEW_DIR + name)
			inner.thumbnail(size, Image.ANTIALIAS)
			im.paste(inner, box=(128, 128))
		name = "%s_%s_%s_%s.png" % (scale, plane, 2*x+1, 2*y+1)
		if name in files:
			inner = Image.open(NEW_DIR + name)
			inner.thumbnail(size, Image.ANTIALIAS)
			im.paste(inner, box=(128, 0))
		name = "%s_%s_%s_%s.png" % (scale, plane, 2*x, 2*y+1)
		if name in files:
			inner = Image.open(NEW_DIR + name)
			inner.thumbnail(size, Image.ANTIALIAS)
			im.paste(inner, box=(0, 0))
		im.save(NEW_DIR + "%s_%s_%s_%s.png" % (scale-1, plane, x, y), "PNG")
		